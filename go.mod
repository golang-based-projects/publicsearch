module gitlab.com/ivanbulyk/publicsearch

go 1.19

require (
	github.com/elastic/go-elasticsearch/v8 v8.4.0
	github.com/mottaquikarim/esquerydsl v0.0.0-20220725035144-d87f6844c615
)

require github.com/elastic/elastic-transport-go/v8 v8.1.0 // indirect
