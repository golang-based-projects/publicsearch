# Publicsearch

Technical challenge

Publicsearch app collects millions of social media messages every day in bulk. Customers create
“cases” where they can add their queries. After the case it’s set up, it starts receiving the
messages that match those queries. Imagine you need to create a service that handles such
matching tasks. This service receives messages as inputs and assigns them to the correct
customer case.
Assumptions:
- For the purposes of the exercise assume you have a list of input messages (see
example input messages)
- The service has access to a list of all cases (see example cases file)
- One message can match to zero, one or multiple cases
- The service outputs to another list the messages matched to the respective case or
cases.
User queries contain terms and can handle multiple operators to combine them. Matching on
terms follows these rules:
- term1 AND term2 - This will return only results that contain term1 and term2 in the
same message, regardless of placement.
- term1 OR term2 - This will return messages that contain term1 or term2, or both.
- (term1 OR term2) AND term3 - The system allows for grouping using brackets. This will
match all messages that contain term1 or term2 or both, and always term3.
- Terms are case insensitive, so Soccer matches soccer
- A term matches only if it exactly matches a whole word. So soc and söccer don't match
soccer. 
Given these requirements, please create the code that outputs matches to another file or
list.